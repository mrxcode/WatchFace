package com.hupp.laurence.watchface.config;
// Created by Laurence Hupp on 22.10.2017.



import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.wearable.complications.ComplicationHelperActivity;
import android.support.wearable.complications.ComplicationProviderInfo;
import android.support.wearable.complications.ProviderChooserIntent;
import android.support.wearable.complications.ProviderInfoRetriever;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.hupp.laurence.watchface.R;
import com.hupp.laurence.watchface.watchface.AnalogComplicationsWatchFaceService;

import java.util.concurrent.Executors;

/**
 * The watch-side config activity for {@link AnalogComplicationsWatchFaceService}, which allows for setting
 * the left and right complications of watch face.
 */
public class AnalogComplicationConfigActivity extends Activity implements View.OnClickListener {

    private static final String TAG = "ConfigActivity";

    static final int COMPLICATION_CONFIG_REQUEST_CODE = 1001;

    /**
     * Used by associated watch face ({@link AnalogComplicationsWatchFaceService}) to let this
     * configuration Activity know which complication locations are supported, their ids, and
     * supported complication data types.
     */
    // TODO: Step 3, intro 1
    public enum ComplicationLocation {
        LEFT,
        RIGHT,
        TOP,
        BOTTOM
    }

    private int mLeftComplicationId;
    private int mRightComplicationId;
    private int mBottomComplicationId;
    private int mTopComplicationId;

    // Selected complication id by user.
    private int mSelectedComplicationId;

    // ComponentName used to identify a specific service that renders the watch face.
    private ComponentName mWatchFaceComponentName;

    // Required to retrieve complication data from watch face for preview.
    private ProviderInfoRetriever mProviderInfoRetriever;

    private ImageView mLeftComplicationBackground;
    private ImageView mRightComplicationBackground;
    private ImageView mBottomComplicationBackground;
    private ImageView mTopComplicationBackground;

    private ImageButton mLeftComplication;
    private ImageButton mRightComplication;
    private ImageButton mBottomComplication;
    private ImageButton mTopComplication;

    private Drawable mDefaultAddComplicationDrawable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_config);

        initializeComplications();

        mProviderInfoRetriever =
                new ProviderInfoRetriever(getApplicationContext(), Executors.newCachedThreadPool());
        mProviderInfoRetriever.init();

        retrieveInitialComplicationsData();

    }

    private void initializeComplications(){
        mDefaultAddComplicationDrawable = getDrawable(R.drawable.add_complication);

        mSelectedComplicationId = -1;

        mLeftComplicationId =
                AnalogComplicationsWatchFaceService.getComplicationId(ComplicationLocation.LEFT);
        mRightComplicationId =
                AnalogComplicationsWatchFaceService.getComplicationId(ComplicationLocation.RIGHT);
        mTopComplicationId =
                AnalogComplicationsWatchFaceService.getComplicationId(ComplicationLocation.TOP);
        mBottomComplicationId =
                AnalogComplicationsWatchFaceService.getComplicationId(ComplicationLocation.BOTTOM);

        mWatchFaceComponentName =
                new ComponentName(getApplicationContext(), AnalogComplicationsWatchFaceService.class);


        // Sets up left complication preview.
        mLeftComplicationBackground = (ImageView) findViewById(R.id.left_complication_background);
        mLeftComplication = (ImageButton) findViewById(R.id.left_complication);
        mLeftComplication.setOnClickListener(this);

        // Sets default as "Add Complication" icon.
        mLeftComplication.setImageDrawable(mDefaultAddComplicationDrawable);
        mLeftComplicationBackground.setVisibility(View.INVISIBLE);

        // Sets up right complication preview.
        mRightComplicationBackground = (ImageView) findViewById(R.id.right_complication_background);
        mRightComplication = (ImageButton) findViewById(R.id.right_complication);
        mRightComplication.setOnClickListener(this);

        // Sets default as "Add Complication" icon.
        mRightComplication.setImageDrawable(mDefaultAddComplicationDrawable);
        mRightComplicationBackground.setVisibility(View.INVISIBLE);

        // Sets up right complication preview.
        mTopComplicationBackground = (ImageView) findViewById(R.id.top_complication_background);
        mTopComplication = (ImageButton) findViewById(R.id.top_complication);
        mTopComplication.setOnClickListener(this);

        // Sets default as "Add Complication" icon.
        mTopComplication.setImageDrawable(mDefaultAddComplicationDrawable);
        mTopComplicationBackground.setVisibility(View.INVISIBLE);

        // Sets up bottom complication preview.
        mBottomComplicationBackground = (ImageView) findViewById(R.id.bottom_complication_background);
        mBottomComplication = (ImageButton) findViewById(R.id.bottom_complication);
        mBottomComplication.setOnClickListener(this);

        // Sets default as "Add Complication" icon.
        mBottomComplication.setImageDrawable(mDefaultAddComplicationDrawable);
        mBottomComplicationBackground.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mProviderInfoRetriever.release();
    }

    // TODO: Step 3, retrieve complication data
    public void retrieveInitialComplicationsData() {
        final int[] complicationIds = AnalogComplicationsWatchFaceService.getComplicationIds();

        mProviderInfoRetriever.retrieveProviderInfo(
                new ProviderInfoRetriever.OnProviderInfoReceivedCallback() {
                    @Override
                    public void onProviderInfoReceived(
                            int watchFaceComplicationId,
                            @Nullable ComplicationProviderInfo complicationProviderInfo) {

                        Log.d(TAG, "\n\nonProviderInfoReceived: " + complicationProviderInfo);

                        updateComplicationViews(watchFaceComplicationId, complicationProviderInfo);
                    }
                },
                mWatchFaceComponentName,
                complicationIds);
    }

    @Override
    public void onClick(View view) {
        if (view.equals(mLeftComplication)) {
            Log.d(TAG, "Left Complication click()");
            launchComplicationHelperActivity(ComplicationLocation.LEFT);

        } else if (view.equals(mRightComplication)) {
            Log.d(TAG, "Right Complication click()");
            launchComplicationHelperActivity(ComplicationLocation.RIGHT);
        }
        if (view.equals(mTopComplication)) {
            Log.d(TAG, "Top Complication click()");
            launchComplicationHelperActivity(ComplicationLocation.TOP);

        } else if (view.equals(mBottomComplication)) {
            Log.d(TAG, "Bottom Complication click()");
            launchComplicationHelperActivity(ComplicationLocation.BOTTOM);
        }
    }

    // Verifies the watch face supports the complication location, then launches the helper
    // class, so user can choose their complication data provider.
    // TODO: Step 3, launch data selector
    private void launchComplicationHelperActivity(ComplicationLocation complicationLocation) {
        mSelectedComplicationId =
                AnalogComplicationsWatchFaceService.getComplicationId(complicationLocation);

        if (mSelectedComplicationId >= 0) {

            int[] supportedTypes =
                    AnalogComplicationsWatchFaceService.getSupportedComplicationTypes(
                            complicationLocation);

            startActivityForResult(
                    ComplicationHelperActivity.createProviderChooserHelperIntent(
                            getApplicationContext(),
                            mWatchFaceComponentName,
                            mSelectedComplicationId,
                            supportedTypes),
                    AnalogComplicationConfigActivity.COMPLICATION_CONFIG_REQUEST_CODE);

        } else {
            Log.d(TAG, "Complication not supported by watch face.");
        }
    }

    public void updateComplicationViews(
            int watchFaceComplicationId, ComplicationProviderInfo complicationProviderInfo) {
        Log.d(TAG, "updateComplicationViews(): id: " + watchFaceComplicationId);
        Log.d(TAG, "\tinfo: " + complicationProviderInfo);

        if (watchFaceComplicationId == mLeftComplicationId) {
            if (complicationProviderInfo != null) {
                mLeftComplication.setImageIcon(complicationProviderInfo.providerIcon);
                mLeftComplicationBackground.setVisibility(View.VISIBLE);

            } else {
                mLeftComplication.setImageDrawable(mDefaultAddComplicationDrawable);
                mLeftComplicationBackground.setVisibility(View.INVISIBLE);
            }

        } else if (watchFaceComplicationId == mRightComplicationId) {
            if (complicationProviderInfo != null) {
                mRightComplication.setImageIcon(complicationProviderInfo.providerIcon);
                mRightComplicationBackground.setVisibility(View.VISIBLE);

            } else {
                mRightComplication.setImageDrawable(mDefaultAddComplicationDrawable);
                mRightComplicationBackground.setVisibility(View.INVISIBLE);
            }
        } else if (watchFaceComplicationId == mTopComplicationId) {
            if (complicationProviderInfo != null) {
                mTopComplication.setImageIcon(complicationProviderInfo.providerIcon);
                mTopComplicationBackground.setVisibility(View.VISIBLE);

            } else {
                mTopComplication.setImageDrawable(mDefaultAddComplicationDrawable);
                mTopComplicationBackground.setVisibility(View.INVISIBLE);
            }
        } else if (watchFaceComplicationId == mBottomComplicationId) {
            if (complicationProviderInfo != null) {
                mBottomComplication.setImageIcon(complicationProviderInfo.providerIcon);
                mBottomComplicationBackground.setVisibility(View.VISIBLE);

            } else {
                mBottomComplication.setImageDrawable(mDefaultAddComplicationDrawable);
                mBottomComplicationBackground.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == COMPLICATION_CONFIG_REQUEST_CODE && resultCode == RESULT_OK) {

            // Retrieves information for selected Complication provider.
            ComplicationProviderInfo complicationProviderInfo =
                    data.getParcelableExtra(ProviderChooserIntent.EXTRA_PROVIDER_INFO);
            Log.d(TAG, "Provider: " + complicationProviderInfo);

            if (mSelectedComplicationId >= 0) {
                updateComplicationViews(mSelectedComplicationId, complicationProviderInfo);
            }
        }
    }
}
