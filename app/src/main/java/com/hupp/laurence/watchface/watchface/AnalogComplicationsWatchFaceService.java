package com.hupp.laurence.watchface.watchface;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.wearable.complications.ComplicationData;
import android.support.wearable.complications.ComplicationHelperActivity;
import android.support.wearable.complications.rendering.ComplicationDrawable;
import android.support.wearable.watchface.CanvasWatchFaceService;
import android.support.wearable.watchface.WatchFaceStyle;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.WindowInsets;

import com.hupp.laurence.watchface.R;
import com.hupp.laurence.watchface.config.AnalogComplicationConfigActivity;

import java.util.Calendar;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

//Created by Laurence Hupp on 21.10.2017.


public class AnalogComplicationsWatchFaceService extends CanvasWatchFaceService {

    /*
     * Update rate in milliseconds for interactive mode. We update once a second to advantage the
     * second hand.
     */
    private static final long INTERACTIVE_UPDATE_RATE_MS = TimeUnit.SECONDS.toMillis(1);

    private static final int LEFT_COMPLICATION_ID = 0;
    private static final int RIGHT_COMPLICATION_ID = 1;
    private static final int TOP_COMPLICATION_ID = 2;
    private static final int BOTTOM_COMPLICATION_ID = 3;

    private static final int[] COMPLICATION_IDS = {
            LEFT_COMPLICATION_ID,
            RIGHT_COMPLICATION_ID,
            TOP_COMPLICATION_ID,
            BOTTOM_COMPLICATION_ID};

    // Left and right dial supported types.
    private static final int[][] COMPLICATION_SUPPORTED_TYPES = {
            {
                    ComplicationData.TYPE_RANGED_VALUE,
                    ComplicationData.TYPE_ICON,
                    ComplicationData.TYPE_SHORT_TEXT,
                    ComplicationData.TYPE_SMALL_IMAGE
            },
            {
                    ComplicationData.TYPE_RANGED_VALUE,
                    ComplicationData.TYPE_ICON,
                    ComplicationData.TYPE_SHORT_TEXT,
                    ComplicationData.TYPE_SMALL_IMAGE
            },
            {
                    ComplicationData.TYPE_RANGED_VALUE,
                    ComplicationData.TYPE_ICON,
                    ComplicationData.TYPE_SHORT_TEXT,
                    ComplicationData.TYPE_SMALL_IMAGE
            },
            {
                    ComplicationData.TYPE_RANGED_VALUE,
                    ComplicationData.TYPE_ICON,
                    ComplicationData.TYPE_SHORT_TEXT,
                    ComplicationData.TYPE_SMALL_IMAGE
            }
    };

    public static int getComplicationId(
            AnalogComplicationConfigActivity.ComplicationLocation complicationLocation) {
        switch (complicationLocation) {
            case LEFT:
                return LEFT_COMPLICATION_ID;
            case RIGHT:
                return RIGHT_COMPLICATION_ID;
            case TOP:
                return TOP_COMPLICATION_ID;
            case BOTTOM:
                return BOTTOM_COMPLICATION_ID;
            default:
                return -1;
        }
    }

    public static int[] getComplicationIds() {
        return COMPLICATION_IDS;
    }

    public static int[] getSupportedComplicationTypes(
            AnalogComplicationConfigActivity.ComplicationLocation complicationLocation) {
        switch (complicationLocation) {
            case LEFT:
                return COMPLICATION_SUPPORTED_TYPES[0];
            case RIGHT:
                return COMPLICATION_SUPPORTED_TYPES[1];
            case TOP:
                return COMPLICATION_SUPPORTED_TYPES[2];
            case BOTTOM:
                return COMPLICATION_SUPPORTED_TYPES[3];
            default:
                return new int[] {};
        }
    }

    @Override
    public Engine onCreateEngine(){
        /* provide own watch face implementation */
        return new Engine();
    }

    /* implement service callback methods */
    private class Engine extends CanvasWatchFaceService.Engine {

        private static final int MSG_UPDATE_TIME = 0;

        private static final float HOUR_STROKE_WIDTH = 10f;
        private static final float MINUTE_STROKE_WIDTH = 8f;
        private static final float SECOND_TICK_STROKE_WIDTH = 3f;

        private static final float CENTER_GAP_AND_CIRCLE_RADIUS = 10f;

        private final float COMPLICATION_RADIUS = 4.5f;

        private Calendar calendar;

        // graphic objects
        private Paint backgroundPaint;
        private Paint hourPaint;
        private Paint minutePaint;
        private Paint secondAndHighlightPaint;
        private Paint tickAndCirclePaint;

        private int backgroundColor;
        private int watchHourColor;
        private int watchMinuteColor;
        private int watchSecondAndHighlightColor;

        private boolean ambient;
        private boolean lowBitAmbient;
        private boolean burnInProtection;
        private boolean hasFlatTire;

        private boolean registeredTimeZoneReceiver = false;

        private float centerX;
        private float centerY;

        private float secondHandLength;
        private float minuteHandLength;
        private float hourHandLength;



        /* Maps active complication ids to the data for that complication. Note: Data will only be
        * present if the user has chosen a provider via the settings activity for the watch face.
        */
        private SparseArray<ComplicationData> activeComplicationDataSparseArray;

        /* Maps complication ids to corresponding ComplicationDrawable that renders the
         * the complication data on the watch face.
         */
        private SparseArray<ComplicationDrawable> complicationDrawableSparseArray;

        // handler to update the time once a second in interactive mode
        final Handler updateTineHandler = new Handler(){
            @Override
            public void handleMessage(Message message){
                switch (message.what){
                    case MSG_UPDATE_TIME:
                        invalidate();
                        if (shouldTimerBeRunning()){
                            long timeMs = System.currentTimeMillis();
                            long delayMs = INTERACTIVE_UPDATE_RATE_MS
                                    - (timeMs % INTERACTIVE_UPDATE_RATE_MS);
                            updateTineHandler.sendEmptyMessageDelayed(MSG_UPDATE_TIME, delayMs);
                        }
                        break;
                }
            }
        };

        private final BroadcastReceiver timeZoneReceiver =
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        calendar.setTimeZone(TimeZone.getDefault());
                        invalidate();
                    }
                };


        @Override
        public void onCreate(SurfaceHolder holder){
            super.onCreate(holder);
            // configure the system UI
            setWatchFaceStyle(new WatchFaceStyle.Builder(AnalogComplicationsWatchFaceService.this)
                    .setBackgroundVisibility(WatchFaceStyle.BACKGROUND_VISIBILITY_INTERRUPTIVE)
                    .setShowSystemUiTime(false)
                    .setAcceptsTapEvents(true)
                    .build());

            initializeBackground();

            initializeComplications();

            initializeWatchFace();

            // allocate a Calendar to calculate local time using the UTC time and time zone
            calendar = Calendar.getInstance();
        }
        private void initializeBackground(){

            initializeColors();

            // Initialize background color
            backgroundPaint = new Paint();
            backgroundPaint.setColor(backgroundColor);

        }

        private void initializeComplications(){
            activeComplicationDataSparseArray = new SparseArray<>(COMPLICATION_IDS.length);

            ComplicationDrawable leftComplicationDrawable =
                    (ComplicationDrawable) getDrawable(R.drawable.custom_complication_styles);
            leftComplicationDrawable.setContext(getApplicationContext());

            ComplicationDrawable rightComplicationDrawable =
                    (ComplicationDrawable) getDrawable(R.drawable.custom_complication_styles);
            rightComplicationDrawable.setContext(getApplicationContext());

            ComplicationDrawable topComplicationDrawable =
                    (ComplicationDrawable) getDrawable(R.drawable.custom_complication_styles);
            topComplicationDrawable.setContext(getApplicationContext());

            ComplicationDrawable bottomComplicationDrawable =
                    (ComplicationDrawable) getDrawable(R.drawable.custom_complication_styles);
            bottomComplicationDrawable.setContext(getApplicationContext());

            complicationDrawableSparseArray = new SparseArray<>(COMPLICATION_IDS.length);
            complicationDrawableSparseArray.put(LEFT_COMPLICATION_ID, leftComplicationDrawable);
            complicationDrawableSparseArray.put(RIGHT_COMPLICATION_ID, rightComplicationDrawable);
            complicationDrawableSparseArray.put(TOP_COMPLICATION_ID, topComplicationDrawable);
            complicationDrawableSparseArray.put(BOTTOM_COMPLICATION_ID, bottomComplicationDrawable);

            setActiveComplications(COMPLICATION_IDS);
        }

        private void initializeWatchFace(){

            hourPaint = new Paint();
            hourPaint.setColor(watchHourColor);
            hourPaint.setStrokeWidth(HOUR_STROKE_WIDTH);
            hourPaint.setAntiAlias(true);
            hourPaint.setStrokeCap(Paint.Cap.SQUARE);

            minutePaint = new Paint();
            minutePaint.setColor(watchMinuteColor);
            minutePaint.setStrokeWidth(MINUTE_STROKE_WIDTH);
            minutePaint.setAntiAlias(true);
            minutePaint.setStrokeCap(Paint.Cap.SQUARE);

            secondAndHighlightPaint = new Paint();
            secondAndHighlightPaint.setColor(watchSecondAndHighlightColor);
            secondAndHighlightPaint.setStrokeWidth(SECOND_TICK_STROKE_WIDTH);
            secondAndHighlightPaint.setAntiAlias(true);
            secondAndHighlightPaint.setStrokeCap(Paint.Cap.SQUARE);

            tickAndCirclePaint = new Paint();
            tickAndCirclePaint.setColor(watchHourColor);
            tickAndCirclePaint.setStrokeWidth(SECOND_TICK_STROKE_WIDTH);
            tickAndCirclePaint.setAntiAlias(true);
            tickAndCirclePaint.setStrokeCap(Paint.Cap.ROUND);
        }

        private void initializeColors(){
            backgroundColor = Color.BLACK;
            watchHourColor = Color.WHITE;
            watchMinuteColor = Color.WHITE;
            watchSecondAndHighlightColor = Color.CYAN;
        }
        @Override
        public void onPropertiesChanged(Bundle properties){
            super.onPropertiesChanged(properties);
            lowBitAmbient = properties.getBoolean(PROPERTY_LOW_BIT_AMBIENT, false);
            burnInProtection = properties.getBoolean(PROPERTY_BURN_IN_PROTECTION,
                    false);

            ComplicationDrawable complicationDrawable;

            for (int i = 0; i < COMPLICATION_IDS.length; i++) {
                complicationDrawable = complicationDrawableSparseArray.get(COMPLICATION_IDS[i]);

                if(complicationDrawable != null) {
                    complicationDrawable.setLowBitAmbient(lowBitAmbient);
                    complicationDrawable.setBurnInProtection(burnInProtection);
                }
            }
        }

        @Override
        public void onApplyWindowInsets(WindowInsets insets) {
            super.onApplyWindowInsets(insets);

            hasFlatTire = insets.getSystemWindowInsetBottom() > 0;
        }

        @Override
        public void onAmbientModeChanged(boolean inAmbientMode) {

            super.onAmbientModeChanged(inAmbientMode);

            ambient = inAmbientMode;

            updateWatchPaintStyles();

            ComplicationDrawable complicationDrawable;
            for (int COMPLICATION_ID : COMPLICATION_IDS) {
                complicationDrawable = complicationDrawableSparseArray.get(COMPLICATION_ID);
                complicationDrawable.setInAmbientMode(ambient);
            }

            updateTimer();
        }

        private void updateWatchPaintStyles(){
            if(ambient){
                backgroundPaint.setColor(Color.BLACK);

                hourPaint.setColor(Color.WHITE);
                minutePaint.setColor(Color.WHITE);
                secondAndHighlightPaint.setColor(Color.WHITE);
                tickAndCirclePaint.setColor(Color.WHITE);

                hourPaint.setAntiAlias(false);
                minutePaint.setAntiAlias(false);
                secondAndHighlightPaint.setAntiAlias(false);
                tickAndCirclePaint.setAntiAlias(false);


            } else {
                initializeWatchFace();
            }
        }

        @Override
        public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height){

            /*
             * Finds the coordinates of the center point on the screen, and ignore the window
             * insets, so that, on round watches with a "chin", the watch fac is centered on the
             * entire screen, not just the usable portion.
             */
            centerX = width / 2f;
            centerY = height / 2f;

            /*
             * Calculate lengths of different hands based on watch screen size.
             */
            secondHandLength = (float) (centerX * 0.875);
            minuteHandLength = (float) (centerX * 0.75);
            hourHandLength = (float) (centerX * 0.5);

            int sizeOfComplication = width / 4;
            int midpointOfScreen = width / 2;

            int horizontalOffset = (midpointOfScreen - sizeOfComplication) / 2;
            int verticalOffset = midpointOfScreen - (sizeOfComplication / 2);

            final float offset = hasFlatTire ? -18f : -10f; //offset for complications

            Rect leftBounds =
                    createComplicationRect(centerX / 2 - offset, centerY,
                            COMPLICATION_RADIUS);

            ComplicationDrawable leftComplicationDrawable =
                    complicationDrawableSparseArray.get(LEFT_COMPLICATION_ID);
            leftComplicationDrawable.setBounds(leftBounds);

            Rect rightBounds =
                    createComplicationRect(centerX * 1.5f + offset, centerY,
                            COMPLICATION_RADIUS);
            ComplicationDrawable rightComplicationDrawable =
                    complicationDrawableSparseArray.get(RIGHT_COMPLICATION_ID);
            rightComplicationDrawable.setBounds(rightBounds);

            Rect topBounds = createComplicationRect(centerX, centerY / 2 - offset,
                    COMPLICATION_RADIUS);
            ComplicationDrawable topComplicationDrawable =
                    complicationDrawableSparseArray.get(TOP_COMPLICATION_ID);
            topComplicationDrawable.setBounds(topBounds);

            Rect bottomBounds =
                    createComplicationRect(centerX, centerY * 1.5f + offset,
                            COMPLICATION_RADIUS);
            ComplicationDrawable bottomComplicationDrawable =
                    complicationDrawableSparseArray.get(BOTTOM_COMPLICATION_ID);
            bottomComplicationDrawable.setBounds(bottomBounds);


        }

        /**
         * Creates the rectangles objects for the complication to be placed in
         *
         * @param centerX       x coordinate of the center
         * @param centerY       y coordinate of the center
         * @param desiredRadius radius to use for dividing by the mCenterX coordinate
         * @return the complication rectangle
         */
        private Rect createComplicationRect(float centerX, float centerY, float desiredRadius) {
            final int radius = Math.round(this.centerX / desiredRadius);

            final int centerXInt = Math.round(centerX);
            final int centerYInt = Math.round(centerY);

            return new Rect(centerXInt - radius,
                    centerYInt - radius,
                    centerXInt + radius,
                    centerYInt + radius);
        }

        @Override
        public void onTimeTick(){
            super.onTimeTick();

            invalidate();
        }

        @Override
        public void onDraw(Canvas canvas, Rect bounds){

            long now = System.currentTimeMillis();
            calendar.setTimeInMillis(now);

            // drawing code here
            drawBackground(canvas);

            drawComplications(canvas, now);

            drawWatchFace(canvas);

        }

        private void drawBackground(Canvas canvas){

            if (ambient && (lowBitAmbient || burnInProtection)){
                canvas.drawColor(Color.BLACK);
            } else {
                canvas.drawColor(backgroundColor);
            }
        }

        private void drawWatchFace(Canvas canvas){

            float innerTickRadius = centerX - 10;
            float outerTickRadius = centerX;
            for (int tickIndex = 0; tickIndex < 12; tickIndex++){
                float tickRot = (float) (tickIndex * Math.PI * 2 / 12);
                float innerX = (float) Math.sin(tickRot) * innerTickRadius;
                float innerY = (float) -Math.cos(tickRot) * innerTickRadius;
                float outerX = (float) Math.sin(tickRot) * outerTickRadius;
                float outerY = (float) -Math.cos(tickRot) * outerTickRadius;
                canvas.drawLine(
                        centerX + innerX,
                        centerY + innerY,
                        centerX + outerX,
                        centerY + outerY,
                        tickAndCirclePaint);
            }

            /*
             * These calculations reflect the rotation in degrees per unit of time, e.g.,
             * 360 / 60 = 6 and 360 / 12 = 30
             */
            final float seconds =
                    (calendar.get(Calendar.SECOND) + calendar.get(Calendar.MILLISECOND) / 1000f);
            final float secondsRotation = seconds * 6f;
            final float minutesRotation = calendar.get(Calendar.MINUTE) * 6f;
            final float hourHandOffset = calendar.get(Calendar.MINUTE) / 2f;
            final float hoursRotation = (calendar.get(Calendar.HOUR) * 30) + hourHandOffset;

            /*
             * Save the canvas state before we can begin to rotate it.
             */
            canvas.save();

            canvas.rotate(hoursRotation, centerX, centerY);
            canvas.drawLine(
                    centerX,
                    centerY - CENTER_GAP_AND_CIRCLE_RADIUS * 2,
                    centerX,
                    centerY - hourHandLength,
                    hourPaint
            );

            canvas.rotate(minutesRotation - hoursRotation, centerX, centerY);
            canvas.drawLine(
                    centerX,
                    centerY - CENTER_GAP_AND_CIRCLE_RADIUS * 2,
                    centerX,
                    centerY - minuteHandLength,
                    minutePaint);

            /*
             * Ensure the "seconds" hand is drawn only when we are in interactive mode.
             * Otherwise, we only update the watch face once a minute.
             */
            if (!ambient) {
                canvas.rotate(secondsRotation - minutesRotation, centerX, centerY);
                canvas.drawLine(
                        centerX,
                        centerY - CENTER_GAP_AND_CIRCLE_RADIUS * 2,
                        centerX,
                        centerY - secondHandLength,
                        secondAndHighlightPaint);
            }
            canvas.drawCircle(
                    centerX, centerY, CENTER_GAP_AND_CIRCLE_RADIUS, tickAndCirclePaint);

            /* Restore the canvas' original orientation. */
            canvas.restore();
        }

        private void drawComplications(Canvas canvas, long currentTimeMillis) {
            // TODO: Step 4, drawComplications()
            int complicationId;
            ComplicationDrawable complicationDrawable;

            for (int COMPLICATION_ID : COMPLICATION_IDS) {
                complicationId = COMPLICATION_ID;
                complicationDrawable = complicationDrawableSparseArray.get(complicationId);
                complicationDrawable.setContext(AnalogComplicationsWatchFaceService.this.getApplicationContext());
                complicationDrawable.draw(canvas, currentTimeMillis);

            }
        }

        @Override
        public void onVisibilityChanged(boolean visible){
            super.onVisibilityChanged(visible);

            if (visible){
                registerReceiver();
                // Update time zone and date formats, in case they changed while we weren't visible
                calendar.setTimeZone(TimeZone.getDefault());
            } else {
                unregisterReceiver();
            }
            invalidate();
            updateTimer();
        }

        private void registerReceiver() {
            if (registeredTimeZoneReceiver) {
                return;
            }
            registeredTimeZoneReceiver = true;
            IntentFilter filter = new IntentFilter(Intent.ACTION_TIMEZONE_CHANGED);
            AnalogComplicationsWatchFaceService.this.registerReceiver(timeZoneReceiver, filter);
        }

        private void unregisterReceiver() {
            if (!registeredTimeZoneReceiver) {
                return;
            }
            registeredTimeZoneReceiver = false;
            AnalogComplicationsWatchFaceService.this.unregisterReceiver(timeZoneReceiver);
        }

        /**
         * Starts/stops the {@link #updateTineHandler} timer based on the state of the watch face.
         */
        private void updateTimer() {
            updateTineHandler.removeMessages(MSG_UPDATE_TIME);
            if (shouldTimerBeRunning()) {
                updateTineHandler.sendEmptyMessage(MSG_UPDATE_TIME);
            }
        }

        /**
         * Returns whether the {@link #updateTineHandler} timer should be running. The timer should
         * only run in active mode.
         */
        private boolean shouldTimerBeRunning(){
            return isVisible() && !ambient;
        }

        @Override
        public void onComplicationDataUpdate(
                int complicationId, ComplicationData complicationData) {
            // Adds/updates active complication data in the array.
            activeComplicationDataSparseArray.put(complicationId, complicationData);

            // Updates correct ComplicationDrawable with updated data.
            ComplicationDrawable complicationDrawable =
                    complicationDrawableSparseArray.get(complicationId);
            complicationDrawable.setComplicationData(complicationData);
            invalidate();
        }

        @Override
        public void onTapCommand(int tapType, int x, int y, long eventTime) {

            switch (tapType) {
                case TAP_TYPE_TAP:
                    int tappedComplicationId = getTappedComplicationId(x, y);
                    if (tappedComplicationId != -1) {
                        onComplicationTap(tappedComplicationId);
                    }
                    break;
            }
        }

        /*
         * Determines if tap inside a complication area or returns -1.
         */
        private int getTappedComplicationId(int x, int y) {

            int complicationId;
            ComplicationData complicationData;
            ComplicationDrawable complicationDrawable;

            long currentTimeMillis = System.currentTimeMillis();

            for (int COMPLICATION_ID : COMPLICATION_IDS) {
                complicationId = COMPLICATION_ID;
                complicationData = activeComplicationDataSparseArray.get(complicationId);

                if ((complicationData != null)
                        && (complicationData.isActive(currentTimeMillis))
                        && (complicationData.getType() != ComplicationData.TYPE_NOT_CONFIGURED)
                        && (complicationData.getType() != ComplicationData.TYPE_EMPTY)) {

                    complicationDrawable = complicationDrawableSparseArray.get(complicationId);
                    Rect complicationBoundingRect = complicationDrawable.getBounds();

                    if (complicationBoundingRect.width() > 0) {
                        if (complicationBoundingRect.contains(x, y)) {
                            return complicationId;
                        }
                    }
                }
            }
            return -1;
        }

        // Fires PendingIntent associated with complication (if it has one).
        private void onComplicationTap(int complicationId) {

            ComplicationData complicationData =
                    activeComplicationDataSparseArray.get(complicationId);

            if (complicationData != null) {

                if (complicationData.getTapAction() != null) {
                    try {
                        complicationData.getTapAction().send();
                    } catch (PendingIntent.CanceledException e) {
                        // handle event
                    }

                } else if (complicationData.getType() == ComplicationData.TYPE_NO_PERMISSION) {

                    // Watch face does not have permission to receive complication data, so launch
                    // permission request.
                    ComponentName componentName = new ComponentName(
                            getApplicationContext(),
                            AnalogComplicationsWatchFaceService.class);

                    Intent permissionRequestIntent =
                            ComplicationHelperActivity.createPermissionRequestHelperIntent(
                                    getApplicationContext(), componentName);

                    startActivity(permissionRequestIntent);
                }

            }
        }

    }

}
